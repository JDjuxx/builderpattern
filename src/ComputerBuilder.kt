open class YugiohCardBuilder {
    private var CardType:String? = null
    private var Attack:Int? = null
    private var Defense:Int? = null
    private var NameCard:String? = null
    private var Description:String? = null

    fun setCardType(CardType:String):YugiohCardBuilder{
        this.CardType = CardType
        return this
    }
    fun getCardType():String?{
        return this.CardType
    }
    fun setAttack(Attack:Int):YugiohCardBuilder{
        this.Attack = Attack
        return this
    }
    fun getAttack():Int?{
        return this.Attack
    }
    fun setDefense(Defense:Int):YugiohCardBuilder{
        this.Defense = Defense
        return this
    }
    fun getDefense():Int?{
        return this.Defense
    }
    fun setNameCard(NameCard:String):YugiohCardBuilder{
        this.NameCard = NameCard
        return this
    }
    fun getNameCard():String?{
        return this.NameCard
    }
    fun setDescription(Description:String):YugiohCardBuilder{
        this.Description = Description
        return this
    }
    fun getDescription():String?{
        return this.Description
    }
}

class YugiohCard{

    var cb:YugiohCardBuilder
    constructor(compBuild:YugiohCardBuilder) {
        this.cb = compBuild
    }

    fun build():String{

        return ("Su carta ha sido creada con los siguientes datos :" +
                "\nNombre de la Carta : ${cb.getNameCard()} " +
                "\nTipo de Carta : ${cb.getCardType()} "+
                "\nAtaque : ${cb.getAttack()} " +
                "\nDefensa : ${cb.getDefense()} " +
                "\nDescripcion : ${cb.getDescription()}"
                )
    }
}

fun main(args: Array<String>) {
    var compBuilder = YugiohCardBuilder()
    compBuilder.setCardType("Guerrero/Efecto").setAttack(1200).setDefense(2000).setDescription("En cada ronda su ataque aumenta en 100").setNameCard("Guerrero Pantera")
    // not setting any value for external mouse

    var comp = YugiohCard(compBuilder)
    println(comp.build())
}

